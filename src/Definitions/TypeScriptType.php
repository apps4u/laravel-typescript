<?php

namespace Based\TypeScript\Definitions;

use ReflectionMethod;

class TypeScriptType
{
    public const STRING  = 'string';
    public const NUMBER  = 'number';
    public const BOOLEAN = 'boolean';
    public const ANY     = 'any';
    public const NULL    = 'null';

    public static function fromMethod(ReflectionMethod $method): array
    {
        //$types = $method->getReturnType() instanceof ReflectionUnionType
        // ? $method->getReturnType()->getTypes()
        //  : (string) $method->getReturnType();
        $types = $method->getReturnType();

        if (is_string($types) && strpos($types, '?') !== false) {
            $types = [
                str_replace('?', '', $types),
                self::NULL
            ];
        }

        return collect($types)
            ->map(function (string $type) {
                $match = null;
                switch ($type) {
                    case 'float':
                    case  'int':
                        $match = self::NUMBER;
                        break;

                    case    'string':
                        $match = self::STRING;
                        break;
                    case      'array':
                        $match = self::array();
                        break;
                    case          'object':
                        $match = self::ANY;
                        break;
                    case    'null':
                        $match = self::NULL;
                        break;
                    case       'bool':
                        $match = self::BOOLEAN;
                        break;
                    default :
                        $match = self::ANY;
                };
                return $match;
            })
            ->toArray();
    }

    public static function array(string $type = self::ANY): string
    {
        return "Array<{$type}>";
    }
}
