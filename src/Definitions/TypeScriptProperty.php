<?php

namespace Based\TypeScript\Definitions;

use Illuminate\Support\Collection;

class TypeScriptProperty
{
    public string $name;
        public  $types;
        public bool $optional = false;
        public bool $readonly = false;
        public bool $nullable = false;
    public function __construct(
        $name,
        $types,
        $optional = false,
         $readonly = false,
         $nullable = false
    )
    {
    }

    public function getTypes(): string
    {
        return collect($this->types)
            ->when($this->nullable, fn(Collection $types) => $types->push(TypeScriptType::NULL))
            ->join(' | ', '');
    }

    public function __toString(): string
    {
        return collect($this->name)
            ->when($this->readonly, fn(Collection $definition) => $definition->prepend('readonly '))
            ->when($this->optional, fn(Collection $definition) => $definition->push('?'))
            ->push(': ')
            ->push($this->getTypes())
            ->push(';')
            ->join('');
    }
}
