<?php

namespace Based\TypeScript\Commands;

use Based\TypeScript\TypeScriptGenerator;
use Illuminate\Console\Command;

class TypeScriptGenerateCommand extends Command
{
    public $signature = 'typescript:generate';

    public $description = 'Generate TypeScript definitions from PHP classes';

    public function handle()
    {
        $generator = new TypeScriptGenerator(
           config('typescript.generators', []),
             config('typescript.paths', []),
             config('typescript.output', resource_path('js/models.d.ts')),
             config('typescript.autoloadDev', false),
        );

        $generator->execute();

        $this->comment('TypeScript definitions generated successfully');
    }
}
